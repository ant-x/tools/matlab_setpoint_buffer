#!/usr/bin/env python
import rospy
#from std_msgs.msg import String
from mavros_msgs.msg import PositionTarget
from mavros_msgs.msg import AttitudeTarget
from mavros_antx.msg import PoseTarget
from mavros_antx.msg import LMNFinjection

class SetpointBuffer:

    TOPIC_POS_IN = 'matlab/setpoint_raw/local';
    TOPIC_POS_OUT = 'mavros/setpoint_raw/local';

    TOPIC_ATT_IN = 'matlab/setpoint_raw/attitude';
    TOPIC_ATT_OUT = 'mavros/setpoint_raw/attitude';

    TOPIC_POSE_IN = 'matlab/pose_setpoint';
    TOPIC_POSE_OUT = 'mavros/pose_setpoint';

    TOPIC_LMNF_INJ = 'mavros/LMNF_injection';

    MSG_TYPE_POS = 0;
    MSG_TYPE_ATT = 1;
    MSG_TYPE_POSE = 2;

    STATE_IDLE = 0
    STATE_POS = 1
    STATE_ATT = 2
    STATE_POSE = 3;

    def __init__(self, rate_sp, timeout_idle):
        self.rate_sp = rate_sp

        self.timeout_idle = timeout_idle
        enableTimeoutIdle = True
        if (timeout_idle == 0):
            enableTimeoutIdle = False
            rospy.loginfo(rospy.get_name() + ' disabling idle timeout')

        self.lastSetpointData_pos = PositionTarget()
        self.lastSetpointData_att = AttitudeTarget()
        self.lastSetpointData_pose = PoseTarget()
        self.lastSetpointType = self.MSG_TYPE_POS
        self.lastTime = rospy.Time.now() - rospy.Duration(self.timeout_idle) # in this way we start in IDLE with flagTimeout true
        self.state = self.STATE_IDLE

        # create an idle setpoint message
        self.idlePositionSetpoint = PositionTarget()
        self.idlePositionSetpoint.coordinate_frame = 1 # this corresponds to FRAME_LOCAL_NED
        self.idlePositionSetpoint.type_mask = 1<<14 # idle setpoint mask

        self.lastSetpointData_pos = self.idlePositionSetpoint

        rospy.Subscriber(self.TOPIC_POS_IN, PositionTarget, self.callback_pos)
        rospy.Subscriber(self.TOPIC_ATT_IN, AttitudeTarget, self.callback_att)
        rospy.Subscriber(self.TOPIC_POSE_IN, PoseTarget, self.callback_pose)
        rospy.Subscriber(self.TOPIC_LMNF_INJ, LMNFinjection, self.callback_lmnf)

        self.pub_pos = rospy.Publisher(self.TOPIC_POS_OUT, PositionTarget, queue_size=1)
        self.pub_att = rospy.Publisher(self.TOPIC_ATT_OUT, AttitudeTarget, queue_size=1)
        self.pub_pose = rospy.Publisher(self.TOPIC_POSE_OUT, PoseTarget, queue_size=1)

        rate = rospy.Rate(self.rate_sp)

        while not rospy.is_shutdown():
            flagTimeout = rospy.Time.now() - self.lastTime > rospy.Duration(self.timeout_idle)
            if (enableTimeoutIdle == False):
                flagTimeout = False

            self.update_state_machine(flagTimeout)

            if (self.state == self.STATE_IDLE):
                pubData = self.idlePositionSetpoint
                self.pub_pos.publish(pubData)

            if (self.state == self.STATE_POS):
                pubData = self.lastSetpointData_pos
                self.pub_pos.publish(pubData)

            if (self.state == self.STATE_ATT):
                pubData = self.lastSetpointData_att
                self.pub_att.publish(pubData)

            if (self.state == self.STATE_POSE):
                pubData = self.lastSetpointData_pose
                self.pub_pose.publish(pubData)

            rate.sleep()

    def update_state_machine(self, flagTimeout):
        currState = self.state
        lastSetpointType = self.lastSetpointType

        isLastSetpointTypePos = (lastSetpointType == self.MSG_TYPE_POS)
        isLastSetpointTypeAtt = (lastSetpointType == self.MSG_TYPE_ATT)
        isLastSetpointTypePose = (lastSetpointType == self.MSG_TYPE_POSE)

        if (currState == self.STATE_IDLE):
            # evaluate transition from IDLE to POS
            if (isLastSetpointTypePos == True and flagTimeout == False):
                self.state = self.STATE_POS
                rospy.loginfo(rospy.get_name() + ' state transition from IDLE to POS')

            # evaluate transition from IDLE to ATT
            if (isLastSetpointTypeAtt == True and flagTimeout == False):
                self.state = self.STATE_ATT
                rospy.loginfo(rospy.get_name() + ' state transition from IDLE to ATT')

            # evaluate transition from IDLE to POSE
            if (isLastSetpointTypePose == True and flagTimeout == False):
                self.state = self.STATE_POSE
                rospy.loginfo(rospy.get_name() + ' state transition from IDLE to POSE')

        if (currState == self.STATE_POS):
            # evaluate transition from POS to IDLE
            if (flagTimeout == True):
                self.state = self.STATE_IDLE
                rospy.loginfo(rospy.get_name() + ' timeout detected')
                rospy.loginfo(rospy.get_name() + ' state transition from POS to IDLE')

            # evaluate transition from POS to ATT
            if (isLastSetpointTypeAtt == True and flagTimeout == False):
                self.state = self.STATE_ATT
                rospy.loginfo(rospy.get_name() + ' state transition from POS to ATT')

            # evaluate transition from POS to POSE
            if (isLastSetpointTypePose == True and flagTimeout == False):
                self.state = self.STATE_POSE
                rospy.loginfo(rospy.get_name() + ' state transition from POS to POSE')

        if (currState == self.STATE_ATT):
            # evaluate transition from ATT to IDLE
            if (flagTimeout == True):
                self.state = self.STATE_IDLE
                rospy.loginfo(rospy.get_name() + ' timeout detected')
                rospy.loginfo(rospy.get_name() + ' state transition from ATT to IDLE')

            # evaluate transition from ATT to POS
            if (isLastSetpointTypePos == True and flagTimeout == False):
                self.state = self.STATE_POS
                rospy.loginfo(rospy.get_name() + ' state transition from ATT to POS')

            # evaluate transition from ATT to POSE
            if (isLastSetpointTypePose == True and flagTimeout == False):
                self.state = self.STATE_POSE
                rospy.loginfo(rospy.get_name() + ' state transition from ATT to POSE')

        if (currState == self.STATE_POSE):
            # evaluate transition from POSE to IDLE
            if (flagTimeout == True):
                self.state = self.STATE_IDLE
                rospy.loginfo(rospy.get_name() + ' timeout detected')
                rospy.loginfo(rospy.get_name() + ' state transition from POSE to IDLE')

            # evaluate transition from POSE to POS
            if (isLastSetpointTypePos == True and flagTimeout == False):
                self.state = self.STATE_POS
                rospy.loginfo(rospy.get_name() + ' state transition from POSE to POS')

            # evaluate transition from POSE to ATT
            if (isLastSetpointTypeAtt == True and flagTimeout == False):
                self.state = self.STATE_ATT
                rospy.loginfo(rospy.get_name() + ' state transition from POSE to ATT')

    def callback_pos(self, data):
        rospy.loginfo(rospy.get_caller_id() + "POS setpoint message received!")
        self.lastSetpointData_pos = data
        self.lastSetpointType = self.MSG_TYPE_POS
        self.lastTime = rospy.Time.now()

    def callback_att(self, data):
        rospy.loginfo(rospy.get_caller_id() + "ATT setpoint message received!")
        self.lastSetpointData_att = data
        self.lastSetpointType = self.MSG_TYPE_ATT
        self.lastTime = rospy.Time.now()

    def callback_pose(self, data):
        rospy.loginfo(rospy.get_caller_id() + "POSE setpoint message received!")
        self.lastSetpointData_pose = data
        self.lastSetpointType = self.MSG_TYPE_POSE
        self.lastTime = rospy.Time.now()

    def callback_lmnf(self, data):
        rospy.loginfo(rospy.get_caller_id() + "LMNFinjection message received!")
        self.lastTime = rospy.Time.now()

if __name__ == '__main__':
    try:
        rospy.init_node('matlab_setpoint_buffer', anonymous=True)

        rate_sp = rospy.get_param('~rate_sp')
        timeout_idle = rospy.get_param('~timeout_idle')

        SetpointBuffer(rate_sp, timeout_idle)

    except rospy.ROSInterruptException:
        pass
