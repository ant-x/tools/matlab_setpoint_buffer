from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    scripts=['scripts/matlab_setpoint_buffer.py']
)

setup(**d)
