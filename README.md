# matlab_setpoint_buffer

A ROS node to receive setpoint commands from MATLAB and re-send them at constant rate to MAVROS.

The matlab_setpoint_buffer node is an integration to the DroneCmd package, which allows to command drone from MATLAB.

## Setup
Create a catkin workspace. Install the packages with ``catkin_make_isolated``.
TODO add more details

## Dependencies
This package depends on the ANT-X customized version of MAVROS https://gitlab.com/ant-x/tools/mavros

## Instructions
The matlab_setpoint_buffer node is automatically started together with the ROS master node (roscore) and the MATLAB global ROS node from MATLAB when launching the ``Drone2DOF.startup()`` command in the DroneCmd package. See the documentation of DroneCmd.

Alternatively, you can launch the node manually with ``roslaunch matlab_setpoint_buffer matlabSetpoint.launch``

## Launch file setup
You can setup the launchfile in the ``launch/`` directory with the parameters explained in the following.

The same parameters can also be specified when running ``roslaunch`` from the command line; however, it is more convenient to use the launchfile provided.

## Parameters
You can customize the behaviour of matlab_setpoint_buffer by means of the following parameters:

* ``ns`` the namespace under which the node will be created (this must correspond to the namespace in MAVROS and when creating the Drone2DOF object in MATLAB);
* ``rate_sp`` the matlab_setpoint_buffer node will oversample the last setpoint received from MATLAB at this constant setpoint rate (default 50Hz);
* ``timeout_idle`` if no setpoint is received from the user for an amount of time larger than the timeout specified by this parameter (default 20s), matlab_setpoint_buffer will send idle setpoints (i.e., the motors will spin at minimum speed) until a new command is received.

# ROS topics
matlab_setpoint_buffer acts as an interface between the MATLAB (global) ROS node and MAVROS.

Both the MATLAB (global) ROS node, matlab_setpoint_buffer and MAVROS must live under the same namespace for the system to work properly.

matlab_setpoint_buffer maps topics according to the following scheme ``[SOURCE NODE]<incoming topic name>--><outcoming topic name>[DESTINATION NODE]``:

* Position setpoints: ``[MATLAB GLOBAL NODE]<matlab/setpoint_raw/local>--><mavros/setpoint_raw/local>[MAVROS]``
* Attitude setpoints: ``[MATLAB GLOBAL NODE]<matlab/setpoint_raw/attitude>--><mavros/setpoint_raw/attitude>[MAVROS]``

Further, the matlab_setpoint_buffer node listens to ``mavros/LMNF_injection``. In this way, when performing identification experiments, the drone does not enter into idle mode even if no setpoint command is sent by the user.

(notice that the topic names are relative wrt the namespace)
